---
title: Home
---

Since October 2023, I am a postdoc in the group of [Eike Lau](https://www.math.uni-bielefeld.de/~lau/) at Bielefeld University.
Previously I was a PhD-student at the University Duisburg-Essen under the supervision of [Ulrich Görtz](https://esaga.uni-due.de/ulrich.goertz/).