---
title: Old things
---

# SS2024: Kleine-AG Normed fiber functors

The plan for the next Kleine AG is to learn about Paul Ziegler's preprint [Normed fiber functors](https://arxiv.org/abs/2203.17073) that gives a Tannakian description of the Bruhat-Tits building of an unramified reductive group.

- **Time:** 13th of April (a Saturday).
- **Place:** Seminar room N-U-3.05 in the math department in Essen (see also [here](https://www.esaga.net/ulrich.goertz/howtogetthere/)).

[Here](https://makoba.gitlab.io/ss2024-kleine-ag-normed-fiber-functors/normed-fiber-functors-program.pdf) is a link to the current version of the program.

### Schedule

- 10.00 - 10.30: *Arrival*
- 10.30 - 11.40: **Representations of algebraic groups (Sarah Diana Meier)**
- 11.40 - 12.00: *Break*
- 12.00 - 13.10: **Normed vector spaces and the Tannakian building (Thiago Solovera e Nery)**
- 13.10 - 15.30: *Break (lunch)*
- 15.30 - 16.40: **Stabilizer groups (Sriram Chinthalagiri Venkata)**
- 16.40 - 17.20: *Break/Deciding on topic for the next Kleine AG*
- 17.20 - 18.30: **Comparison (Giulio Marazza)**

# WS2022/2023: PhD-Seminar Algebraic Groups

This seminar is organized by Niklas Müller and myself.

- **Time:** Wednesday from 4-6 pm. The first meeting is on the 12th of October.
- **Place:** Seminar room N-U-4.05 in the WSC.

[Here](https://makoba.gitlab.io/ws2022-phd-seminar-program/phd-seminar-program.pdf) is a link to the current version of the seminar program.

## Notes:
- [Talk 1:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-1.pdf) Generalities 1: Sheaves, Algebraic Groups, Basic Constructions (by Anneloes Viergever).
- [Talk 2:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-2.pdf) Generalities 2: More Basic Constructions, Hopf Algebras (by Ludvig Modin).
- [Talk 3:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-3.pdf) Generalities 3: Faithful Flatness for Hopf Algebras, Representations of Algebraic Groups (by Guillermo Gamarra).
- [Talk 4:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-4.pdf) Generalities 4: Algebraic Group Actions, Quotients of Algebraic Groups (by Pietro Gigli).
- [Talk 5:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-5.pdf) Solvable Groups 1 (by Riccardo Tosi).
- [Talk 6:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-6.pdf) Diagonalizable Groups (by Giulio Marazza).
- [Talk 7:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-7.pdf) Unipotent Groups (by Federica Santi).
- [Talk 8:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-8.pdf) Solvable Groups 2 (by Niklas Müller).
- [Talk 10:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-10.pdf) Borel Subgroups and Maximal Tori 2 (by Lukas Bröring)
- [Talk 11:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-11.pdf) Reductive Groups 1: Reductive Groups of Semisimple Rank 1 (by Luca Marannino)
- [Talk 13:](https://makoba.gitlab.io/winter-2022/phd-seminar-talk-13.pdf) Abstract Root Data (by Bence Forrás)


# WS2021/2022: PhD-Seminar Shimura Varieties

The seminar is organized by Aryaman Patel and myself.

- **Time:** Monday from 2-4 pm. The first meeting is on the 11th of October.
- **Place:** Seminar room S-U-4.02 in the WSC.

[Here](https://makoba.gitlab.io/ws2021-phd-seminar-program/phd-seminar-program.pdf) is a link to the current version of the seminar program.

## Notes:
- [Talk 1:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-1.pdf) Algebraic Groups and Lie Groups (by Paulina Fust).
- [Talk 2:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-2.pdf) Hermitian Symmetric Domains (by Ravjot Kohli).
- [Talk 3:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-3.pdf) Locally Symmetric Varieties (by Alessandro D'Angelo).
- [Talk 4:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-4.pdf) Connected Shimura Varieties (by Robin Witthaus).
- [Talk 6:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-6.pdf) Shimura Varieties 2 (by Jonas Franzel).
- [Talk 8:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-8.pdf) Hodge Structures and Abelian Schemes over \\(\mathbf{C}\\) (by Anneloes Viergever).
- [Talk 9:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-9.pdf) Siegel Modular Variety 1 (by Ludvig Modin).
- [Talk 10:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-10.pdf) Canonical Models of Shimura Varieties 1 (by Marc Kohlhaw).
- [Talk 11:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-11.pdf) Canonical Models of Shimura Varieties 2 (by Chirantan Chowdhury).
- [Talk 12:](https://makoba.gitlab.io/winter-2021/phd-seminar-talk-12.pdf) Siegel Modular Variety 2 (by Luca Marannino).