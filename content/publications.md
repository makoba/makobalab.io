---
title: Publications and preprints
---

- Sebastian Bartling and Manuel Hoff, Moduli spaces of nilpotent displays,  
Preprint, 2024, arxiv: [2404.10132](https://arxiv.org/abs/2404.10132).
- Manuel Hoff, On parahoric \\((\mathcal{G}, \mu)\\)-displays,  
Preprint, 2023. arxiv: [2311.00127](https://arxiv.org/abs/2311.00127).
- Manuel Hoff, The EKOR-stratification on the Siegel modular variety with parahoric level structure,  
Preprint, 2022. arxiv: [2206.07470](https://arxiv.org/abs/2206.07470).

Here is a link to my [PhD-thesis](https://duepublico2.uni-due.de/receive/duepublico_mods_00079070).