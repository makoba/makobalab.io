---
title: Teaching
---

# SS2025: Simpliziale Homotopietheorie

**Termine:**
- Montag 14-16, B2-212
- Freitag 12-14, B2-260a (Übung)

[Hier](https://makoba.gitlab.io/ss2025-simplicial-homotopy-theory/overview.pdf) ist eine grobe Übersicht über den Kursinhalt.